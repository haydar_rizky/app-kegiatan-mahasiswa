package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by user on 11/13/2015.
 */
public class CustomAdapter_seminar_skrg extends ArrayAdapter<data_seminar>  {

    String PATH = "http://ukm-mobile.esy.es/images/seminar/";
    public CustomAdapter_seminar_skrg(Context context, List<data_seminar> data) {
        super(context, R.layout.activity_row_seminar_skrg, data);
    }

    @Override
    public View getView(int position , View convertView , ViewGroup parent)
    {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.activity_row_seminar_skrg, parent, false);


        TextView judul = (TextView) customView.findViewById(R.id.textjdlseminar);
        TextView tanggal = (TextView) customView.findViewById(R.id.texttglseminar);
        TextView jam = (TextView) customView.findViewById(R.id.textjamseminar);
        ImageView gambar = (ImageView) customView.findViewById(R.id.imageView8);

        data_seminar dataSeminar = getItem(position);

        judul.setText(dataSeminar.getJudul());
        tanggal.setText(dataSeminar.getTanggal());
        jam.setText(dataSeminar.getWaktu());
        Picasso.with(getContext()).load(PATH+dataSeminar.getSpanduk()).resize(100,100).into(gambar);

        return customView;
    }
}
