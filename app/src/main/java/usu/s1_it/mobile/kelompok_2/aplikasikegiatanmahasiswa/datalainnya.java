package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 1/12/2016.
 */


    import java.util.ArrayList;
    import java.util.List;
    import com.google.gson.annotations.Expose;
    import com.google.gson.annotations.SerializedName;


    public class datalainnya {

        @SerializedName("data_lainnya")
        @Expose
        private List<data_lainnya> dataLainnya = new ArrayList<data_lainnya>();

        /**
         *
         * @return
         * The dataLainnya
         */
        public List<data_lainnya> getDataLainnya() {
            return dataLainnya;
        }

        /**
         *
         * @param dataLainnya
         * The data_lainnya
         */
        public void setDataLainnya(List<data_lainnya> dataLainnya) {
            this.dataLainnya = dataLainnya;
        }

    }
