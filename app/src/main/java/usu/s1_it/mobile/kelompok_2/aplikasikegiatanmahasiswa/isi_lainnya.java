package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.NavUtils;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class isi_lainnya extends AppCompatActivity {
    String PATH = "http://ukm-mobile.esy.es/images/lainnya/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi_lainnya);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView Judul = (TextView) findViewById(R.id.jdllain);
        TextView Tanggal = (TextView) findViewById(R.id.texttgllain2);
        TextView Jam = (TextView) findViewById(R.id.textjamlain2);
        TextView Lokasi = (TextView) findViewById(R.id.textlokasilain2);
        TextView Deskripsi = (TextView) findViewById(R.id.textdeslain2);
        ImageView Gambar = (ImageView) findViewById(R.id.imagelain);
        TextView Contact = (TextView) findViewById(R.id.textcplain2);
        Intent intent = getIntent();

        Judul.setText(intent.getStringExtra("Judul"));
        Tanggal.setText(intent.getStringExtra("Tanggal"));
        Jam.setText(intent.getStringExtra("Jam"));
        Lokasi.setText(intent.getStringExtra("Lokasi"));
        Deskripsi.setText(intent.getStringExtra("Deskripsi"));
        Contact.setText(intent.getStringExtra("Contact Person"));
        Picasso.with(this).load(PATH + intent.getStringExtra("Gambar")).into(Gambar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_isi_lainnya, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
