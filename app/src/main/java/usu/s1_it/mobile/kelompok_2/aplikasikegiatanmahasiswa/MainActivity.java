package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout drawerLayout;
    private ListView navList;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    String User,Pass;
    private static final String ROOT_URL = "http://ukm-mobile.esy.es/";

    public MainActivity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout =(DrawerLayout)findViewById(R.id.drawerlayout);
        navList = (ListView)findViewById(R.id.navlist);
        ArrayList<String> navArray = new ArrayList<String>();
        navArray.add("Beranda");
        navArray.add("Kegiatan Sekarang");
        navArray.add("List Kegiatan");
        navArray.add("Tentang Kami");
        navArray.add("Log in");
        navArray.add("Keluar");
        navList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_activated_1,navArray);
        navList.setAdapter(adapter);
        navList.setOnItemClickListener(this);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.opendrawer,R.string.closedrawer);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) {
            throw new AssertionError();
        } else {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            fragmentManager = getSupportFragmentManager();

            loadSelection(0);
        }


    }

    private void loadSelection(int i){
        navList.setItemChecked(i, true);

        switch (i){
            case 0:
                beranda beranda = new beranda();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder,beranda);
                fragmentTransaction.commit();
                break;
            case 1:
                kegiatan kegiatan = new kegiatan();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder,kegiatan);
                fragmentTransaction.commit();
                break;
            case 2:
                list_kegiatan list_kegiatan = new list_kegiatan();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder,list_kegiatan);
                fragmentTransaction.commit();
                break;
            case 3:
                tentang_kami tentang_kami = new tentang_kami();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder,tentang_kami);
                fragmentTransaction.commit();
                break;
            case 4:
                login login = new login();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder,login);
                fragmentTransaction.commit();
                break;
            case 5:
                new AlertDialog.Builder(this)
                        .setTitle("Konfirmasi")
                        .setMessage("Apakah Anda yakin ingin keluar dari aplikasi ini?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton("Tidak", null)
                        .show();
                break;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id == android.R.id.home){
            if (drawerLayout.isDrawerOpen(navList)){
                drawerLayout.closeDrawer(navList);
            }else{
                drawerLayout.openDrawer(navList);
            }
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        loadSelection(position);
        drawerLayout.closeDrawer(navList);
    }




    public void onAdmin (View view)
    {
        final ProgressDialog pDialog = ProgressDialog
                .show(this, null, "Sedang melakukan verifikasi...", true, false);

        EditText usertxt = (EditText) findViewById(R.id.user);
        EditText passtxt = (EditText) findViewById(R.id.pass);
        User = usertxt.getText().toString();
        Pass = passtxt.getText().toString();

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //setting the root url
                .build(); //finally builder the adapter

        //creating object for our interface
        RegisterAPI api = adapter.create(RegisterAPI.class);
        api.login(User, Pass, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                pDialog.dismiss();
                if (user.user.isEmpty()) {
                    Toast.makeText(MainActivity.this, "username dan password salah", Toast.LENGTH_LONG).show();
                } else if (user.user.get(0).getUsername().equals("Admin1")) {
                    Intent i = new Intent(MainActivity.this, adminbiasa_activity.class);
                    startActivity(i);
                }  else {
                    Intent i = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(i);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(MainActivity.this, "Maaf, untuk sementara server tidak dapat melakukan verifikasi, silakan coba lagi.", Toast.LENGTH_LONG).show();
                pDialog.dismiss();
            }
        });

    }

    public void onClickHIMATIF (View view)
    {
        Intent i = new Intent(this,HIMATIF.class);
        startActivity(i);
    }

    public void onClickSRC (View view)
    {
        Intent i = new Intent(this,ukmi.class);
        startActivity(i);
    }

    public void onClickIMILKOM (View view)
    {
        Intent i = new Intent(this,IMILKOM.class);
        startActivity(i);
    }

    public void onClickPEMA (View view)
    {
        Intent i = new Intent(this,PEMA.class);
        startActivity(i);
    }

    public void onClickSignUp (View view)
    {
        Intent i = new Intent(this,signup.class);
        startActivity(i);
    }




}



