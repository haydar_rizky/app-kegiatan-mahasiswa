package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ukm_lain extends Fragment {

    private static final String ROOT_URL = "http://ukm-mobile.esy.es/";
    Activity context;
    ListView List;
    public ukm_lain() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        // Inflate the layout for this fragment
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //setting the root url
                .build(); //finally builder the adapter

        View view = inflater.inflate(R.layout.fragment_ukm_lain, container, false);
        List = (ListView) view.findViewById(R.id.list_ukm_lain);
        //creating object for our interface
        RegisterAPI api = adapter.create(RegisterAPI.class);
        api.getdataukmlain(new Callback<dataukmlain>() {
            @Override
            public void success(dataukmlain data, Response response) {

                ListAdapter adapter = new CustomAdapter_ukmlain(getActivity(), data.getDataUkmlain()
                );

                List.setAdapter(adapter);
                context = getActivity();

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity().getBaseContext(), "Gagal memuat data, silakan berpindah tab terlebih dahulu untuk memuat ulang.", Toast.LENGTH_LONG).show();
            }
        });

        return List;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
