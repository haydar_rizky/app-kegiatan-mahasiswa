package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class edit_lainnya_proses extends AppCompatActivity implements View.OnClickListener{

    private static final String ROOT_URL = "http://ukm-mobile.esy.es/edit_lainnya.php";

    private TextView Header;
    private EditText Judul;
    private EditText Tanggal;
    private EditText Jam;
    private EditText Lokasi;
    private EditText Deskripsi;
    private EditText Contact;
    private EditText Penyelenggara;

    private Button buttonsubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_lainnya_proses);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Header = (TextView) findViewById(R.id.txtid);
        Judul = (EditText) findViewById(R.id.txtjudul);
        Tanggal = (EditText) findViewById(R.id.txttanggal);
        Lokasi = (EditText) findViewById(R.id.txtlokasi);
        Jam = (EditText) findViewById(R.id.txtjam);
        Deskripsi = (EditText) findViewById(R.id.txtdeskripsi);
        Contact = (EditText) findViewById(R.id.txtcp);
        Penyelenggara = (EditText) findViewById(R.id.txtpenyelenggara);

        Intent intent = getIntent();
        Header.setText(intent.getStringExtra("ID"));
        Judul.setText(intent.getStringExtra("Judul"));
        Tanggal.setText(intent.getStringExtra("Tanggal"));
        Lokasi.setText(intent.getStringExtra("Lokasi"));
        Jam.setText(intent.getStringExtra("Jam"));
        Deskripsi.setText(intent.getStringExtra("Deskripsi"));
        Contact.setText(intent.getStringExtra("Contact Person"));
        Penyelenggara.setText(intent.getStringExtra("Penyelenggara"));


        buttonsubmit = (Button) findViewById(R.id.updatelainnya);
        buttonsubmit.setOnClickListener(this);
    }




    private  void editLainnya(){


        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //setting the root url
                .build(); //finally builder the adapter

        //creating object for our interface
        RegisterAPI api = adapter.create(RegisterAPI.class);

        //tamrin

        //textname =  editTextName.getText().toString();

        //defining the method inertuser of user interface
        api.editLainnya(
                //passing the value by getting it from edit text

                Header.getText().toString(),
                Judul.getText().toString(),
                Tanggal.getText().toString(),
                Lokasi.getText().toString(),
                Jam.getText().toString(),
                Deskripsi.getText().toString(),
                Contact.getText().toString(),
                Penyelenggara.getText().toString(),
                //creating an anonymous callback

                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {
                        //On success we will read the server's output using bufferedreader
                        //Creating a bufferedreader object

                        BufferedReader reader = null;

                        //An string to store output from the server
                        String output = "";

                        try {
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            //Reading the output in the string
                            output = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //Displaying the output as a toast
                        Toast.makeText(edit_lainnya_proses.this, "Data berhasil diubah", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //If any error occured displaying the error as toast
                        Toast.makeText(edit_lainnya_proses.this, "Data gagal diubah", Toast.LENGTH_LONG).show();

                    }
                }

        );
    }


    @Override
    public void onClick(View v) {
        if(v == buttonsubmit)
        {
            editLainnya();
        }

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_lainnya_proses, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
