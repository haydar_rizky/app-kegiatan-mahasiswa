package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 1/4/2016.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class data_ukm {

    @SerializedName("id_ukm")
    @Expose
    private String idUkm;
    @SerializedName("judul")
    @Expose
    private String judul;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;

    /**
     *
     * @return
     * The idUkm
     */
    public String getIdUkm() {
        return idUkm;
    }

    /**
     *
     * @param idUkm
     * The id_ukm
     */
    public void setIdUkm(String idUkm) {
        this.idUkm = idUkm;
    }

    /**
     *
     * @return
     * The judul
     */
    public String getJudul() {
        return judul;
    }

    /**
     *
     * @param judul
     * The judul
     */
    public void setJudul(String judul) {
        this.judul = judul;
    }

    /**
     *
     * @return
     * The logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     *
     * @param logo
     * The logo
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     *
     * @return
     * The deskripsi
     */
    public String getDeskripsi() {
        return deskripsi;
    }

    /**
     *
     * @param deskripsi
     * The deskripsi
     */
    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

}