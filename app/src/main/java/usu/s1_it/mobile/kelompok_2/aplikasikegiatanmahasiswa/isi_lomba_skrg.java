package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.NavUtils;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class isi_lomba_skrg extends AppCompatActivity {
    String PATH = "http://ukm-mobile.esy.es/images/lomba/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi_lomba_skrg);

        final Intent intent = getIntent();
        Button go = (Button) findViewById(R.id.btnuploadl);
        go.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(isi_lomba_skrg.this, upload_foto.class);
                i.putExtra("Id", intent.getStringExtra("Id"));
                startActivity(i);
            }
        });
        Button go2 = (Button) findViewById(R.id.btngaleril);
        go2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(isi_lomba_skrg.this, galeri.class);
                i.putExtra("Id", intent.getStringExtra("Id"));
                startActivity(i);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView Judul = (TextView) findViewById(R.id.judullombas);
        TextView Tema = (TextView) findViewById(R.id.texttemalomba2);
        TextView Jenis = (TextView) findViewById(R.id.textjenislomba2);
        TextView Jadwal = (TextView) findViewById(R.id.textjadwallomba2);
        TextView Mulai = (TextView) findViewById(R.id.texttglmulai2);
        TextView Selesai = (TextView) findViewById(R.id.texttglselesai2);
        TextView Biaya = (TextView) findViewById(R.id.textbiayalomba2);
        TextView Link = (TextView) findViewById(R.id.textlinklomba2);
        TextView Lokasi = (TextView) findViewById(R.id.textlokasilomba2);
        ImageView Gambar = (ImageView) findViewById(R.id.brosurlombas);
        TextView Contact = (TextView) findViewById(R.id.textcplomba2);


        Judul.setText(intent.getStringExtra("Judul"));
        Tema.setText(intent.getStringExtra("Tema"));
        Jenis.setText(intent.getStringExtra("Jenis"));
        Jadwal.setText(intent.getStringExtra("Jadwal"));
        Mulai.setText(intent.getStringExtra("Mulai"));
        Selesai.setText(intent.getStringExtra("Selesai"));
        Biaya.setText(intent.getStringExtra("Biaya"));
        Link.setText(intent.getStringExtra("Link"));
        Lokasi.setText(intent.getStringExtra("Lokasi"));
        Picasso.with(this).load(PATH + intent.getStringExtra("Gambar")).into(Gambar);
        Contact.setText(intent.getStringExtra("Contact Person"));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_isi_lomba, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
