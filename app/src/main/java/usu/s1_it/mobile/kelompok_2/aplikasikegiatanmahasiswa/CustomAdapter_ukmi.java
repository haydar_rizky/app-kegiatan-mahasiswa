package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by user on 1/4/2016.
 */
public class CustomAdapter_ukmi extends ArrayAdapter<data_ukmi> {

    String PATH = "http://ukm-mobile.esy.es/images/ukm/";
    public CustomAdapter_ukmi(Context context, List<data_ukmi> data) {
        super(context, R.layout.activity_row_ukmlain, data);
    }

    @Override
    public View getView(int position , View convertView , ViewGroup parent)
    {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.activity_row_ukmlain, parent, false);


        TextView judul = (TextView) customView.findViewById(R.id.textjdlukmlain);

        data_ukmi dataukmi = getItem(position);

        judul.setText(dataukmi.getJudul());



        return customView;
    }
}
