package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by user on 1/4/2016.
 */
public class CustomAdapter_galeri extends ArrayAdapter<data_galeri> {

    String PATH = "http://ukm-mobile.esy.es/images/seminar/";
    public CustomAdapter_galeri(Context context, List<data_galeri> data) {
        super(context, R.layout.activity_row_galeri, data);
    }

    @Override
    public View getView(int position , View convertView , ViewGroup parent)
    {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.activity_row_galeri, parent, false);


        ImageView gambar = (ImageView) customView.findViewById(R.id.galeri);

        data_galeri datagaleri = getItem(position);

        Picasso.with(getContext()).load(PATH+datagaleri.getFoto()).into(gambar);



        return customView;
    }
}
