package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("user")
    @Expose
    public List<User_> user = new ArrayList<User_>();

    /**
     *
     * @return
     * The user
     */
    public List<User_> getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(List<User_> user) {
        this.user = user;
    }

}