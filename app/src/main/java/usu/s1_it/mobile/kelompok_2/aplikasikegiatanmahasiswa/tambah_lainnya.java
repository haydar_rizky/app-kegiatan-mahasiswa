package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class tambah_lainnya extends AppCompatActivity implements View.OnClickListener{

    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;

    private DatePickerDialog tglDatePickerDialog;

    private SimpleDateFormat dateFormatter;

    private Button btnPilihWaktu;
    private TimePicker timePicker;
    private EditText timeView;
    private Calendar calendar;
    private String format = "";

    private int hour;
    private int min;

    static final int TIME_DIALOG_ID = 98;

    private EditText editTextnama;
    private EditText editTexttgl;
    private EditText editTextlokasi;
    private EditText editTextwaktu;
    private EditText editTextdeskripsi;
    private EditText editTextpenyelenggara;
    private EditText editTextcp;

    private Button buttonsubmit;

    private static final String ROOT_URL = "http://ukm-mobile.esy.es/insert_lainnya.php";

    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_lainnya);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        findViewsById();

        setDateTimeField();

        editTextnama =(EditText) findViewById(R.id.txtnama_lainnya);
        editTexttgl =(EditText) findViewById(R.id.txttgl_lainnya);
        editTextlokasi =(EditText) findViewById(R.id.txtlokasi_lainnya);
        editTextwaktu =(EditText) findViewById(R.id.txtwaktu_lainnya);
        editTextdeskripsi =(EditText) findViewById(R.id.txtdeskripsi_lainnya);
        editTextpenyelenggara =(EditText) findViewById(R.id.txtpenyelenggara_lainnya);

        editTextcp =(EditText) findViewById(R.id.txtcp_lainnya);

        //add listener button
        buttonsubmit = (Button) findViewById(R.id.btnTambahLain);

        buttonsubmit.setOnClickListener(this);

        //konfigurasi time
        timeView = (EditText) findViewById(R.id.txtwaktu_lainnya);
        calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        min = calendar.get(Calendar.MINUTE);
        showTime(hour, min);

        addListenerOnButton();

    }

    private void findViewsById() {
        editTexttgl = (EditText) findViewById(R.id.txttgl_lainnya);
        editTexttgl.setInputType(InputType.TYPE_NULL);
        editTexttgl.requestFocus();
    }

    private void setDateTimeField() {
        editTexttgl.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        tglDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editTexttgl.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }


    @SuppressWarnings("deprecation")
    public void addListenerOnButton() {

        btnPilihWaktu = (Button) findViewById(R.id.btnPilihWaktu);

        btnPilihWaktu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(TIME_DIALOG_ID);

            }

        });

    }


    @SuppressWarnings("deprecation")
    @Override
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                return new TimePickerDialog(this, timePickerListener, hour, min,false);
        }
        return null;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener =   new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
            hour = selectedHour;
            min = selectedMinute;

            // set current time into textview
            //timeView.setText(new StringBuilder().append(hour).append(":").append(min));

            // set current time into timepicker
            //timePicker1.setCurrentHour(hour);
            //timePicker1.setCurrentMinute(min);*/
            showTime(hour, min);

        }
    };

    public void showTime(int hour, int min) {
        if (hour == 0) {
            hour += 12;
            format = "AM";
        }
        else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }
        timeView.setText(new StringBuilder().append(hour).append(" : ").append(min)
                .append(" ").append(format));
    }


    public void onClickUploadFoto_lainnya (View view)
    {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        );
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgDecodableString = cursor.getString(columnIndex);
                cursor.close();
                ImageView imgView = (ImageView) findViewById(R.id.brosur_lainnya);
                // Set the Image in ImageView after decoding the String
                imgView.setImageBitmap(BitmapFactory
                        .decodeFile(imgDecodableString));

            }

            else {
                Toast.makeText(this, "Anda belum memilih foto",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

    }

    private  void insertLainnya(){

        File photo = new File(imgDecodableString);
        TypedFile typedImage = new TypedFile("image/*", photo);

        ////Here we will handle the http request to insert user to mysql db
        //creating a restadapter

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //setting the root url
                .build(); //finally builder the adapter

        //creating object for our interface
        RegisterAPI api = adapter.create(RegisterAPI.class);

        //tamrin

        //textname =  editTextName.getText().toString();

        //defining the method inertuser of user interface
        api.insertLainnya(
                //passing the value by getting it from edit text

                editTextnama.getText().toString(),
                editTexttgl.getText().toString(),
                editTextlokasi.getText().toString(),
                editTextwaktu.getText().toString(),
                editTextdeskripsi.getText().toString(),
                typedImage,
                editTextpenyelenggara.getText().toString(),
                editTextcp.getText().toString(),

                //creating an anonymous callback

                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {
                        //On success we will read the server's output using bufferedreader
                        //Creating a bufferedreader object

                        BufferedReader reader = null;

                        //An string to store output from the server
                        String output = "";

                        try {
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            //Reading the output in the string
                            output = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //Displaying the output as a toast
                        Toast.makeText(tambah_lainnya.this, "Data berhasil diinput", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //If any error occured displaying the error as toast
                        Toast.makeText(tambah_lainnya.this, "Data gagal diinput", Toast.LENGTH_LONG).show();

                    }
                }

        );
    }


    @Override
    public void onClick(View v) {
        if(v == editTexttgl) {
            tglDatePickerDialog.show();
        } else if(v == buttonsubmit)
        {
            insertLainnya();
        }

    }


    public void onBackPressed() {
        super.onBackPressed();
        Intent m=new Intent(tambah_lainnya.this,adminbiasa_activity.class);
        m.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(m);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tambah_lainnya, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}

