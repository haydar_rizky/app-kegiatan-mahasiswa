package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class lomba_skrg extends Fragment {

    private static final String ROOT_URL = "http://ukm-mobile.esy.es/";
    Activity context;
    ListView List;
    public lomba_skrg() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //setting the root url
                .build(); //finally builder the adapter

        View view = inflater.inflate(R.layout.fragment_lomba_skrg, container, false);
        List = (ListView) view.findViewById(R.id.list_lomba_skrg);
        //creating object for our interface
        RegisterAPI api = adapter.create(RegisterAPI.class);
        api.getlomba_skrg(new Callback<datalomba>() {
            @Override
            public void success(final datalomba data, Response response) {

                ListAdapter adapter = new CustomAdapter_lomba_skrg(getActivity(), data.dataLomba);

                List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(getActivity(), isi_lomba_skrg.class);

                        i.putExtra("Judul",data.dataLomba.get(position).getNama());
                        i.putExtra("Tema",data.dataLomba.get(position).getTema());
                        i.putExtra("Jenis", data.dataLomba.get(position).getJenis());
                        i.putExtra("Jadwal", data.dataLomba.get(position).getJadwal());
                        i.putExtra("Mulai", data.dataLomba.get(position).getMulai());
                        i.putExtra("Selesai", data.dataLomba.get(position).getSelesai());
                        i.putExtra("Biaya", data.dataLomba.get(position).getBiaya());
                        i.putExtra("Link", data.dataLomba.get(position).getLink());
                        i.putExtra("Lokasi", data.dataLomba.get(position).getLokasi());
                        i.putExtra("Gambar", data.dataLomba.get(position).getBrosur());
                        i.putExtra("Contact Person", data.dataLomba.get(position).getCp());


                        startActivity(i);
                    }
                });
                List.setAdapter(adapter);
                context = getActivity();

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity().getBaseContext(), "Gagal memuat data, silakan berpindah tab terlebih dahulu untuk memuat ulang.", Toast.LENGTH_LONG).show();
            }
        });

        return List;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
