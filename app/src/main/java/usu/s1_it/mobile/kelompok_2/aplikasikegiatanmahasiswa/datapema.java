package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

/**
 * Created by user on 14-01-16.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class datapema {

    @SerializedName("data_pema")
    @Expose
    private List<data_pema> dataPema = new ArrayList<data_pema>();

    /**
     *
     * @return
     * The dataPema
     */
    public List<data_pema> getDataPema() {
        return dataPema;
    }

    /**
     *
     * @param dataPema
     * The data_pema
     */
    public void setDataPema(List<data_pema> dataPema) {
        this.dataPema = dataPema;
    }

}