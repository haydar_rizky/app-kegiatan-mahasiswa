package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Asus on 17/01/2016.
 */
public class CustomAdapter_EditLainnya extends ArrayAdapter<data_lainnya> {

    String PATH = "http://ukm-mobile.esy.es/images/lainnya/";
    public CustomAdapter_EditLainnya(Context context, List<data_lainnya> data) {
        super(context, R.layout.activity_row_edit_lainnya, data);
    }

    @Override
    public View getView(int position , View convertView , ViewGroup parent)
    {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.activity_row_edit_lainnya, parent, false);


        TextView judul = (TextView) customView.findViewById(R.id.textjdllain);
        TextView tanggal = (TextView) customView.findViewById(R.id.texttgllain);
        TextView jam = (TextView) customView.findViewById(R.id.textjamlain);
        ImageView gambar = (ImageView) customView.findViewById(R.id.imagelainnya);

        data_lainnya datalainnya = getItem(position);

        judul.setText(datalainnya.getJudul());
        tanggal.setText(datalainnya.getTanggal());
        jam.setText(datalainnya.getJam());


        Picasso.with(getContext()).load(PATH+datalainnya.getBrosur()).resize(70,100).into(gambar);



        return customView;
    }
}
