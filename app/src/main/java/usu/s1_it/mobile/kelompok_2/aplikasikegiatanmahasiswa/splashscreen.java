package usu.s1_it.mobile.kelompok_2.aplikasikegiatanmahasiswa;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by acer on 05/01/2016.
 */
public class splashscreen extends Activity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {

            // TODO Auto-generated method stub
            super.onCreate(savedInstanceState);


            setContentView(R.layout.splash);

            Thread timerThread = new Thread(){
                public void run(){
                    try{
                        sleep(3000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }finally{
                        Intent intent = new Intent(splashscreen.this,MainActivity.class);
                        startActivity(intent);
                    }
                }
            };
            timerThread.start();
        }

        @Override
        protected void onPause() {
            // TODO Auto-generated method stub
            super.onPause();
            finish();
        }

    }

